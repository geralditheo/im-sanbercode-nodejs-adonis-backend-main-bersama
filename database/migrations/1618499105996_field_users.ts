import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class FieldUsers extends BaseSchema {
  protected tableName = 'field_user'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.date('play_date_start')
      table.date('play_date_end')
      table.integer('booking_user_id').unsigned().references('id').inTable('users') // users
      table.integer('field_id').unsigned().references('id').inTable('fields') // fields
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
