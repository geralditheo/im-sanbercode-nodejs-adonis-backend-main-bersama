import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'

// 
import Venue from 'App/Models/Venue'
import User from 'App/Models/User'

/** 
*  @swagger
*  definitions:
*    Venue:
*      type: object
*      properties:
*        id:
*          type: integer
*        name:
*          type: string
*        type:
*          type: string
*        venueId:
*          type: integer
*      required:
*        - name
*        - type
*/

export default class Field extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public type: string
  
  @column()
  public venueId: number

  @belongsTo(() => Venue)
  public venue: BelongsTo<typeof Venue>

  @manyToMany(() => User, {
    localKey: 'id',
    pivotForeignKey: 'field_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'booking_user_id'
  })
  public users: ManyToMany<typeof User>
}
