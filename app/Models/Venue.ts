import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany,  } from '@ioc:Adonis/Lucid/Orm'

// 
import Field from 'App/Models/Field'
import Booking from 'App/Models/Booking'

/** 
*  @swagger
*  definitions:
*    Venue:
*      type: object
*      properties:
*        id:
*          type: integer
*        name:
*          type: string
*        address:
*          type: string
*        phone:
*          type: string
*      required:
*        - name
*        - address
*        - phone
*/

export default class Venue extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public address: string

  @column()
  public phone: string

  /////////////////////////////////////////
  @hasMany(() => Field)
  public fields: HasMany<typeof Field>

  @hasMany(() => Booking)
  public bookings: HasMany<typeof Booking>

  
}
