import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field'

export default class FieldsController {
    /**
     * @swagger
     * /api/v1/venue/:venue_id/field:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Field
     *     parameters:
     *     responses:
     *       200:
     *         description: Indexing of field
     *         example:
     *           message: Index Success
     */
    public async index({response}: HttpContextContract){
        const field = await Field.all()
        return response.status(200).json(field)
    }
    /**
     * @swagger
     * /api/v1/venue/:venue_id/field:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Field
     *     parameters:
     *       - name: name
     *         description: name of the field (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: type
     *         description: type of the field (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: venueId
     *         description: venue id of the field (new)
     *         in: path
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: Storing of field
     *         example:
     *           message: Store Success
     */
    public async store({params ,request ,response}: HttpContextContract){
        let name = request.input('name') ;
        let type = request.input('type');
        let venueId = params.venue_id;

        const field = await Field.create({
            name: name ,
            type: type ,
            venueId: venueId
        })

        return response.status(200).json({message: "Store Success", data: field})
    }
    /**
     * @swagger
     * /api/v1/venue/:venue_id/field/:id:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Field
     *     parameters:
     *       - name: id
     *         description: id of the field
     *         in: path
     *         required: false
     *         type: integer
     *     responses:
     *       200:
     *         description: Showing of field
     *         example:
     *           message: Show Success
     */
    public async show({params, response}: HttpContextContract){
        const field = await Field.findOrFail(params.id)
        return response.status(200).json({message: "Show Success", data: field})
    }
    /**
     * @swagger
     * /api/v1/venue/:venue_id/field/:id:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Field
     *     parameters:
     *       - name: id
     *         description: id of the field (new)
     *         in: path
     *         required: false
     *         type: integer
     *       - name: name
     *         description: name of the field (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: type
     *         description: type of the field (new)
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: Updating of field
     *         example:
     *           message: Update Success
     */
    public async update({request, params, response}: HttpContextContract){
        let name = request.input('name') ;
        let type = request.input('type');

        const field = await Field.findOrFail(params.id)
        field.name = name
        field.type = type
        field.venueId = params.id

        await field.save()
        return response.status(200).json({message: "Update Success"})

    }
    /**
     * @swagger
     * /api/v1/venue/:venue_id/field/:id:
     *   delete:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Field
     *     parameters:
     *       - name: id
     *         description: id of the field 
     *         in: path
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: Deleting of field
     *         example:
     *           message: Delete Success
     */
    public async destroy({params, response}: HttpContextContract){
        const field = await Field.findOrFail(params.id)
        await field.delete()

        return response.status(200).json({message: "Destroy Success"})
    }
}
