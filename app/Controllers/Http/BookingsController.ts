import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookingValidator from 'App/Validators/BookingValidator'
import Database from '@ioc:Adonis/Lucid/Database'

// 
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'


export default class BookingsController {
    /**
     * @swagger
     * /api/v1/venue/:venue_id/booking:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Booking
     *     parameters:
     *     responses:
     *       200:
     *         description: Indexing of booking
     *         example:
     *           message: Index Success
     */
    public async index({params, response}: HttpContextContract){
        const bookings = await Database.query().select('*').from('field_user')      
        return response.status(200).json({bookings})
    }

    /**
     * @swagger
     * /api/v1/venue/:venue_id/booking:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Booking
     *     parameters:
     *       - name: field_id
     *         description: name of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: play_date_start
     *         description: address of the venues (new)
     *         in: query
     *         required: false
     *         type: date
     *       - name: play_date_end
     *         description: address of the venues (new)
     *         in: path
     *         required: false
     *         type: date
     *       - name: booking_user_id
     *         description: address of the venues (new)
     *         in: query
     *         required: false
     *         type: integer
     *     responses:
     *       200:
     *         description: Storing of booking
     *         example:
     *           message: Store Success
     */
    public async store({params, request, response}: HttpContextContract){
        let field_id = request.input('field_id')
        let play_date_start = request.input('play_date_start')
        let play_date_end = request.input('play_date_end')

        let booking = await Database.insertQuery().table('field_user').insert({
            play_date_start: play_date_start,
            play_date_end: play_date_end,
            booking_user_id: 1,
            field_id: field_id
        })

        let venue = await Venue.findOrFail(params.venue_id)
        
        return response.status(200).json({message: "Store Success", data: venue, booking : booking})
    }

    /**
     * @swagger
     * /api/v1/venue/:venue_id/booking/:id:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Booking
     *     parameters:
     *       - name: id
     *         description: id of the booking
     *         in: path
     *         required: false
     *         type: integer
     *     responses:
     *       200:
     *         description: Showing of booking
     *         example:
     *           message: Show Success
     */
    public async show({params, response}: HttpContextContract){
        const field = await Field.query().preload('users').where('id', params.id)
        return response.status(200).json({message: "Ok", data: field})
    }

}
