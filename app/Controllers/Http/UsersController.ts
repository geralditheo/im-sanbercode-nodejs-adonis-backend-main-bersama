import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Mail from '@ioc:Adonis/Addons/Mail'
import User from 'App/Models/User'

export default class UsersController {
    /**
    * @swagger
    * /api/v1/login:
    *   post:
    *     tags:
    *       - Authentification
    *     parameters:
    *       - email: email
    *         description: name of the venues (new)
    *         in: query
    *         required: true
    *         type: string
    *       - password: password
    *         description: address of the venues (new)
    *         in: query
    *         required: true
    *         type: string
    *     responses:
    *       200:
    *         description: Login Success
    *       400:
    *         description: Login Failed
    *       
    */

    public async login({request, auth, response}: HttpContextContract){
        const email = request.input('email')
        const password = request.input('password')
        const token = await auth.use('api').attempt(email, password)

        return response.status(200).json({message: "Login Success", token}) 
    }

  /**
    * @swagger
    * /api/v1/register:
    *   post:
    *     tags:
    *       - Authentification
    *     requestBody:
    *       required: true
    *       content: 
    *         application/x-www-form-urlencoded:
    *           schema:
    *             $ref: '#definitions/User'
    *         application/json:
    *           schema:
    *             $ref: '#definitions/User'
    *     responses:
    *       200:
    *         description: user created, verify otp in email
    *       400:
    *         description: invalid request
    *       
    */

    public async register({request, response}: HttpContextContract){
        const name = request.input('name')
        const email = request.input('email')
        const password = request.input('password')

        const user = await User.create({ name, email, password })

        const otp_code = Math.floor(100000  + Math.random() * 900000)

        let saveCde = await Database.table('otp_codes').insert({otp_code: otp_code, user_id: user.id})
        await Mail.send((message) => {
          message
            .from('admin@gmail.com')
            .to(email)
            .subject('Here is your Confirmation')
            .htmlView('emails/otp_verification', { otp_code })

            return response.created({message: 'Register Success, please verify your otp code' })
        })

        return response.created({message: "Register Success"})
    }

  /**
    * @swagger
    * /api/v1/otp:
    *   post:
    *     tags:
    *       - Authentification
    *     parameters:
    *       - otp_code: otp_code
    *         description: name of the venues (new)
    *         in: query
    *         required: true
    *         type: string
    *       - email: email
    *         description: address of the venues (new)
    *         in: query
    *         required: true
    *         type: string
    *       - Authentification
    *     responses:
    *       200:
    *         description: OTP Confirmation, Success
    *       400:
    *         description: Failed Confirmation
    *       
    */

    public async otpVerification({request, response}: HttpContextContract){
        let otp_code = request.input('otp_code')
        let email = request.input('email')

        let user = await User.findBy('email', email)
        let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

        console.log(user?.id, otpCheck?.user_id);
        

        if (user?.id == otpCheck?.user_id){
            user.is_verified = true
            await user?.save()
      
            return response.status(200).json({message: 'Berhasil konfirmasi otp'})
          }
          else{
            return response.status(400).json({message: 'Gagal konfirmasi otp'})
          }
    }
}
