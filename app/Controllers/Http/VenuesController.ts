import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import VenueValidator from 'App/Validators/VenueValidator'
import Database from '@ioc:Adonis/Lucid/Database'
import Venues from 'App/Models/Venue'

export default class VenuesController {
    /**
     * @swagger
     * /api/v1/venue:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venue
     *     parameters:
     *     responses:
     *       200:
     *         description: Indexing of venues
     *         example:
     *           message: Index Success
     */
    public async index({response}: HttpContextContract){
        const venue = await Venues.all()
        return response.status(200).json({message: 'Index Success', data: venue});
    }
    /**
     * @swagger
     * /api/v1/venue:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venue
     *     parameters:
     *       - name: name
     *         description: name of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: address
     *         description: address of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: phone
     *         description: address of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: Storing of venues
     *         example:
     *           message: Store Success
     */
    public async store({request, response}: HttpContextContract){
        // const data = await request.validate(VenueValidator);

        let name = request.input('name') ;
        let address = request.input('address') ;
        let phone = request.input('phone') ;

        const venue = await Venues.create({
            name: name,
            address: address,
            phone: phone
        })

        console.log(venue);
        return response.status(200).json({message: "Store Success"})
    }
    /**
     * @swagger
     * /api/v1/venue/:id:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venue
     *     parameters:
     *       - id: id
     *         description: id of the venues (new)
     *         in: path
     *         required: false
     *         type: integer
     *     responses:
     *       200:
     *         description: Showing of venues
     *         example:
     *           message: Show Success
     */
    public async show({params, response}: HttpContextContract){
        const venue = await Venues.find(params.id)
        await venue?.preload('fields')
        
        return response.status(200).json({message: "berhasil get data venue by id", data: venue});
    }

    /**
     * @swagger
     * /api/v1/venue/:id:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venue
     *     parameters:
     *       - id: id
     *         description: name of the venues (new)
     *         in: path
     *         required: false
     *         type: integer
     *       - name: name
     *         description: name of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: address
     *         description: address of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *       - name: phone
     *         description: address of the venues (new)
     *         in: query
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: Updating of venues
     *         example:
     *           message: Update Success
     */
    public async update({params, request, response}: HttpContextContract){

        let name = request.input('name') ;
        let address = request.input('address') ;
        let phone = request.input('phone') ;

        const venue = await Venues.findOrFail(params.id)
        venue.name = name
        venue.address = address
        venue.phone = phone

        await venue.save()
        return response.status(200).json({message: "Update Success"})
    }

    /**
     * @swagger
     * /api/v1/venue/:id:
     *   delete:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venue
     *     parameters:
     *       - id: id
     *         description: id of the venues (new)
     *         in: path
     *         required: false
     *         type: string
     *     responses:
     *       200:
     *         description: Deleting of venues
     *         example:
     *           message: Delete Success
     */
    public async destroy({params, response}: HttpContextContract){

        const venue = await Venues.findOrFail(params.id)
        await venue.delete()
        return response.status(200).json({message: "Delete Success"})
    }
}
