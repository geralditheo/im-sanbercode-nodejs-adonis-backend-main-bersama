/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'


Route.get('/', async () => {
  return { hello: 'world' }
})

Route.group(()=>{
  Route.resource('venue', 'VenuesController').apiOnly().middleware({'*': 'auth'})
  Route.resource('venue.field', 'FieldsController').apiOnly().middleware({'*': ['auth', 'verify']})
  Route.resource('venue.booking', 'BookingsController').only(['index', 'store', 'show']).middleware({'*': 'auth'})

  // Auth
  Route.post('/login', 'UsersController.login').as('user.login')
  Route.post('/register', 'UsersController.register').as('user.register')
  Route.post('/otp', 'UsersController.otpVerification').as('user.otp_verification')

}).prefix('/api/v1')


Route.get('health', async ({ response }) => {
  const report = await HealthCheck.getReport()
  
  return report.healthy
    ? response.ok(report)
    : response.badRequest(report)
})